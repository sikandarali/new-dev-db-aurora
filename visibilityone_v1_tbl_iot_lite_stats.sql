-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

#SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_iot_lite_stats`
--

DROP TABLE IF EXISTS `tbl_iot_lite_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_iot_lite_stats` (
  `iot_lite_stat_id` bigint NOT NULL AUTO_INCREMENT,
  `avg_rtt` float DEFAULT NULL,
  `lost` float DEFAULT NULL,
  `maxrtt` float DEFAULT NULL,
  `minrtt` float DEFAULT NULL,
  `probe_sent` tinyint(1) NOT NULL DEFAULT '0',
  `received` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(100) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `iot_lite_id` int unsigned DEFAULT '0',
  PRIMARY KEY (`iot_lite_stat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=242486 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_iot_lite_stats`
--

LOCK TABLES `tbl_iot_lite_stats` WRITE;
/*!40000 ALTER TABLE `tbl_iot_lite_stats` DISABLE KEYS */;
INSERT INTO `tbl_iot_lite_stats` VALUES (242480,0.199,0,1,0,5,5,'172.19.3.52','2023-09-20 20:17:52',20),(242481,0,0,0,0,5,5,'172.19.3.52','2023-09-20 20:20:51',20),(242482,0,0,0,0,5,5,'172.19.3.52','2023-09-20 20:21:49',20),(242483,0.199,0,1,0,5,5,'172.19.3.52','2023-09-20 20:22:48',20),(242484,0.199,0,1,0,5,5,'172.19.3.52','2023-09-20 20:23:50',20),(242485,0,20,0,0,5,4,'172.19.3.52','2023-09-20 20:25:46',20);
/*!40000 ALTER TABLE `tbl_iot_lite_stats` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-21 18:18:00
