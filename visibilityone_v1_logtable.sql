-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

#SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `logtable`
--

DROP TABLE IF EXISTS `logtable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logtable` (
  `id` int NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2010 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logtable`
--

LOCK TABLES `logtable` WRITE;
/*!40000 ALTER TABLE `logtable` DISABLE KEYS */;
INSERT INTO `logtable` VALUES (1969,'PLUGIN EXISTS','1'),(1970,'PREVIOUS DISK INFO','[{\"free\": 90430611456, \"drive\": \"C:\", \"total\": 125947867136}]'),(1971,'CURRENT TOTAL SPACE','Total space:105696460800 Free space1061098240'),(1972,'INCIDENT TRIGGER EVENT','INCIDENT TRIGER PERCENT:98 DISK CONSUMED:99'),(1973,'INCIDENT TRIGGER EVENT','PREVIOUS ZOOM INC98 DISK CONSUMED:99'),(1974,'TBL ZOOM INCIDENT INSERT','COMPANY ID36'),(1975,'REACHED END','END IF'),(1976,'7047','video_device_id'),(1977,'PLUGIN EXISTS','1'),(1978,'PREVIOUS DISK INFO','[{\"free\": 90476363776, \"drive\": \"C:\", \"total\": 125947867136}]'),(1979,'CURRENT TOTAL SPACE','Total space:105696460800 Free space1061098240'),(1980,'INCIDENT TRIGGER EVENT','INCIDENT TRIGER PERCENT:98 DISK CONSUMED:99'),(1981,'INCIDENT TRIGGER EVENT','PREVIOUS ZOOM INC98 DISK CONSUMED:99'),(1982,'INCIDENT UPDATE','INCIDENT ID:2419 PULLS COUNT:2'),(1983,'PULLS COUNTER',' Notification created :0'),(1984,'PLUGIN EXISTS','1'),(1985,'PREVIOUS DISK INFO','[{\"free\": 90474156032, \"drive\": \"C:\", \"total\": 125947867136}]'),(1986,'CURRENT TOTAL SPACE','Total space:105696460800 Free space1061098240'),(1987,'INCIDENT TRIGGER EVENT','INCIDENT TRIGER PERCENT:98 DISK CONSUMED:99'),(1988,'INCIDENT TRIGGER EVENT','PREVIOUS ZOOM INC98 DISK CONSUMED:99'),(1989,'INCIDENT UPDATE','INCIDENT ID:2419 PULLS COUNT:2'),(1990,'PULLS COUNTER',' Notification created :0'),(1991,'Zoom Incident id :','2419'),(1992,'NOTIFICATION NO PREVIOUS DISK','NOTIFICATION CREATED'),(1993,'REACHED END','END IF'),(1994,'Zoom get user list limit','500'),(1995,'Zoom get user list offset','0'),(1996,'Zoom get user list servicetype','all'),(1997,'Zoom get user list limit','500'),(1998,'Zoom get user list offset','0'),(1999,'Zoom get user list servicetype','all'),(2000,'Zoom get user list limit','500'),(2001,'Zoom get user list offset','0'),(2002,'Zoom get user list servicetype','all'),(2003,'7047','video_device_id'),(2004,'Zoom get user list limit','500'),(2005,'Zoom get user list offset','0'),(2006,'Zoom get user list servicetype','all'),(2007,'Zoom get user list limit','500'),(2008,'Zoom get user list offset','0'),(2009,'Zoom get user list servicetype','all');
/*!40000 ALTER TABLE `logtable` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-21 18:28:44
